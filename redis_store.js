const Redis = require('ioredis');
// const logger = require('winston');

// basic constructor - Connect to 127.0.0.1:6379
const redis = new Redis();


function createUID() {
  return redis.incr('auction_globals:next_uid');
}

function addToUserAuctions(user, auction) {
  return redis.zadd(`user:${user.id}:auctions`, auction.id);
}

// HELPER FUNCTIONS
function keyForAuction(auction) {
  return `auction:${auction}`;
}

function keyForUsers(auction) {
  return `auction:${auction}:users`;
}

function keyForBids(auction, round, key) {
  return `auction:${auction}:round:${round}:bids:${key}`;
}

// AUCTION

function setAuction(auction_obj) {
  return redis.hmset(keyForAuction(auction_obj.name, JSON.stringify(auction_obj)));
}

function getAuction(auction_obj) {
  return redis.hgetall(keyForAuction(auction_obj.name, JSON.stringify(auction_obj)));
}

// USER

function addUser(auction, user) {
  return redis.sadd(keyForUsers(auction), user.id);
}

function removeUser(auction, user) {
  return redis.srem(keyForUsers(auction), user.id);
}

// BIDDING

function getBids(auction, round, key) {
  return redis.zrange(keyForBids(auction, round, key), 0, -1);
}

function getBid(auction, round, key) {
  return redis.zrange(keyForBids(auction, round, key), 0, -1);
}

function setBid(auction, round, key, user) {
  return redis.zadd(keyForBids(auction, round, key), new Date().getTime(), user.id);
}

// .then( (res) => logger.info('saved high bid from user:%', user.name) )
// .catch( (err) => logger.error('err') );

exports.addToUserAuctions = addToUserAuctions;
exports.getAuction = getAuction;
exports.setAuction = setAuction;
exports.addUser = addUser;
exports.removeUser = removeUser;
exports.setBid = setBid;
exports.getBid = getBid;
exports.getBids = getBids;