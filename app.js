const express = require('express');
const path = require('path');
// var favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('winston');

const app = express();

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


logger.info(process.argv.join(' '));
if (process.argv.length > 2) {
  logger.info('starting process as auction client...');
  const auction_client = require('./auction_client');
  auction_client.startUp();
} else {
  logger.info('starting process as auction runner...');
  const auction_runner = require('./auction_runner');
  auction_runner.startUp();
}

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, __req, res, __next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, __req, res, __next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
  });
});


module.exports = app;
