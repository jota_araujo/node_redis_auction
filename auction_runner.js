const logger = require('winston');
const chalk = require('chalk');
const Redis = require('ioredis');
const redis = new Redis();
const publisher = new Redis();
const subscriber = new Redis();


redis.set('auction_globals:active_auction', 999);

let active_auction;

// helper functions ----------------------------------------------------------

function keyForAttribute(attr) {
  return `auction:${active_auction.id}:${attr}`;
}

function eventChannelKey(auction_id) {
  return `auction:channels:${auction_id}:events`;
}

function requestChannelKey(auction_id) {
  return `auction:channels:${auction_id}:requests`;
}

// publishes a message to the global channel
function publishToGlobal(eventName, _data) {
  const evt = {
    name: eventName,
    data: _data,
  };
  publisher.publish('auction:channels:global', JSON.stringify(evt));
}

// publishes a message to the active auction channel
function publishToAuction(eventName, _data) {
  const evt = {
    name: eventName,
    data: _data,
  };
  publisher.publish(eventChannelKey(active_auction.id), JSON.stringify(evt));
}


// auction lifecycle ---------------------------------------------------------


// starts a new auction upon successful channel subscription to the auction channel
function onSubscribedToAuctionChannel(err, __count) {
  if (err) {
    logger.err(err);
    return;
  }
  logger.info(chalk.bold(`Now Listening for messages on auction channel: ${active_auction.id}`));
  publishToGlobal('auction_start', JSON.stringify(active_auction));
}

// instanciates the new auction and subscribes to the auction pubsub channel on REDIS
function startNextAuction() {
  redis.spop('auction:globals:queue') // TODO: consider using a blocking operation such as BLPOP
  .then((res) => {
    if (res) {
      active_auction = JSON.parse(res);
      // create auction entry in redis
      redis.set(keyForAttribute('name'), active_auction.name);
      redis.set(keyForAttribute('bid'), active_auction.start_price);
      subscriber.subscribe(requestChannelKey(active_auction.id), onSubscribedToAuctionChannel);
    } else {
      logger.info('No queued auctions found in database. Just chillin...');
    }
  })
  .catch((err) => logger.error(err));
}

// tries to start a new auction a successful subscription to the global channel
function onSubscribedToGlobalChannel(err, __count) {
  if (err) {
    logger.err(err);
    return;
  }
  logger.info('Subscribed to the global channel');
  startNextAuction();
}

// validades new participants and broadcasts the join event
function onUserJoinRequest(userId) {
  // TODO: validate participation
  redis.sadd(keyForAttribute('users'), userId, function (err, res) {
    if (err) {
      logger.error('User join error: ', err);
      return;
    }
    if (res) {
      publishToAuction('auction_join', userId);
    }
  });
}

// updates the user list and broadcasts the leave event
function onUserLeaveRequest(__userId) {
  // TODO:
}

// validates new bids, updates the auction state in REDIS  and broadcasts a bid event
function onUserBidRequest(userId, newBid) {
  // TODO: check if user is registered in the Redis set

  redis.get(keyForAttribute('bid'), function (__err, res) {
    const currentBid = parseInt(res, 10);
    if (newBid >= currentBid) {
      // update bid status in db
      redis.set(keyForAttribute('bid'), newBid);
      logger.info(chalk.bold(`A new bid was accepted : ${newBid}`));
      // broadcast new status
      const payload = {
        user_id: userId,
        bid: newBid,
      };
      publishToAuction('auction_bid', JSON.stringify(payload));
    }

  });
}


// process incoming request from connected auction servers
subscriber.on('message', function (channel, payload) {

  const channelName = channel.replace('auction:channels:', '');

  // for now we ignore messages on global
  if (channelName === 'global') return;
  
  logger.info(`Message on ${  channelName} : ${payload}`);

  const event = JSON.parse(payload);
  switch (event.name) {
    case 'request_join':
      onUserJoinRequest(event.user_id);
      break;
    case 'request_leave':
      onUserLeaveRequest(event.user_id);
      break;
    case 'request_bid':
      onUserBidRequest(event.user_id, parseInt(event.bid, 10));
      break;
    default:
      logger.info('Unexpected event:', event);
  }
});

// startup code --------------------------------------------------------------

module.exports.startUp = function () {
  subscriber.subscribe('auction:channels:global', onSubscribedToGlobalChannel);
};
