const WebSocketServer = require('ws').Server;
const url = require('url');
const logger = require('winston');

function wssFactory(httpServer) {

  const wss = new WebSocketServer({ server: httpServer });

  wss.on('connection', function connection(ws) {
    const location = url.parse(ws.upgradeReq.url, true);
    logger.info(location);
    logger.info('Total clients: ', wss.clients.length);
    ws.on('message', function incoming(message) {
      logger.info('received: %s', message);

      ws.send(message);
    })

    return wss;
  });

}

module.exports = wssFactory;

