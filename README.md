# Description:

A PoC of horizontally scaling a Node.js application using Redis.  
Any number of Node instances can connect to an auction runner using Redis pub/sub.  
Application state can also be persisted in Redis so it can survive the event of any Node instance crashing.

# Status

Whip

# Screenshots:

![.](https://bytebucket.org/jota_araujo/node_redis_auction/raw/a7e4b2b8b209f771c7b189700c91399283fc5918/screenshots/user1.png)  

![.](https://bytebucket.org/jota_araujo/node_redis_auction/raw/a7e4b2b8b209f771c7b189700c91399283fc5918/screenshots/user2.png)  

![.](https://bytebucket.org/jota_araujo/node_redis_auction/raw/a7e4b2b8b209f771c7b189700c91399283fc5918/screenshots/runner.png)  

![.](https://bytebucket.org/jota_araujo/node_redis_auction/raw/a7e4b2b8b209f771c7b189700c91399283fc5918/screenshots/client.png)