const logger = require('winston');
const chalk = require('chalk');
const Redis = require('ioredis');
const shortid = require('shortid');
const WebSocketServer = require('ws').Server;
const wss = new WebSocketServer({ port: 8080 });
// const logger = require('winston');

// basic constructor - Connect to 127.0.0.1:6379
// const redis = new Redis();
const publisher = new Redis();
const subscriber = new Redis();
const users = new Map();

function eventChannelKey(auction_id) {
  return `auction:channels:${auction_id}:events`;
}

function requestChannelKey(auction_id) {
  return `auction:channels:${auction_id}:requests`;
}

function findUser(ws) {
  return users.get(ws);
}

function broadcast(message) {
  wss.clients.forEach((client) => client.send(message));
}

function tellOne(client, message) {
  client.send(message);
}

function tellOthers(excluded, message) {
  wss.clients.forEach(function fn(client) {
    if (client !== excluded) client.send(message);
  });
}

function findClientFromUserId(userId) {
  let result = null;
  wss.clients.forEach(function fn(client) {
    const user = findUser(client);
    if (user.id === userId) result = client;
  });
  return result;
}

// ------------ SERVER <-> SERVER COMMUNICATION

function onAuctionStart(auction) {
  // subscribe to auction channel
  subscriber.subscribe(eventChannelKey(auction.id), function (err, __count) {
    if (err) {
      console.error(err);
      return;
    }
    console.log('Now listening for messages from auction: ', auction.id);
    // forward start event to connected all clients
    broadcast(`A new auction started: ${JSON.stringify(auction)}`);
  });
}

function onAuctionEnd(auction) {
  // unsubscribe to auction
  subscriber.unsubscribe(eventChannelKey(auction.id), function (err, __count) {
    if (err) {
      console.error(err);
      return;
    }
    // forward event to connected all clients
    broadcast(`Auction has finished: ${auction.name}`);
  });
}

function onAuctionJoin(userId) {
  // will find a connection only if the user is connected to this server instance
  const client = findClientFromUserId(userId);
  if (client != null) { // will run only if  the bidder is connected to this specific process
    tellOne(client, 'You have succesfully joined the auction');
    tellOthers(client, `${chalk.bold(findUser(client).name)} has joined the auction`);
  } else {
    broadcast(`User ${userId} joined.`);
  }
}

function onAuctionBid(data) {
  // will find a connection only if the user is connected to this server instance
  const client = findClientFromUserId(data.user_id);
  if (client != null) { // will run only if  the bidder is connected to this specific process
    tellOne(client, `You have placed a bid of ${chalk.bold(data.bid)}`);
    tellOthers(client, `${chalk.bold(findUser(client).name)} placed a bid of ${chalk.bold(data.bid)}`);
  } else {
    broadcast(`User ${data.user_id} placed a bid of ${data.bid}`);
  }
}

subscriber.on('message', function (channel, payload) {

  logger.info(chalk.bold(`Received on ${channel}: `), payload);

  const event = JSON.parse(payload);
  switch (event.name) {
    case 'auction_start':
      onAuctionStart(JSON.parse(event.data)); // payload is the auction data
      break;
    case 'auction_end':
      onAuctionEnd(JSON.parse(event.data)); // payload is the auction data
      break;
    case 'auction_join':
      onAuctionJoin(event.data); // payload is the auction data
      break;
    case 'auction_bid':
      onAuctionBid(JSON.parse(event.data)); // payload is the bid data
      break;
    default:
      logger.error('Unexpected event:', event);
  }
});

function subscribeToChannel(name) {
  subscriber.subscribe(name, function (err, __count) {
    if (err) {
      logger.error(err);
      return;
    }
    logger.info('Now subscribed to channel %s', name);
  });
}


// SOCKET CLIENT <-> SERVER COMMUNICATION

// search for open bid key in redis, if found register user as a bidder
function onUserNameRequest(user, input) {
  user.name = input;
  tellOne(findClientFromUserId(user.id), `Welcome, ${chalk.bold(input)}. Your user id is: ${chalk.bold(user.id)}`);
}

// forward request to auction runner process
function onUserJoinRequest(user, auctionId) {
  const evt = {
    name: 'request_join',
    user_id: user.id,
  };
  publisher.publish(requestChannelKey(auctionId), JSON.stringify(evt));
}

// forward request to auction runner process
function onUserLeaveRequest(user, auctionId) {
  const evt = {
    name: 'request_leave',
    user_id: user.id,
  };
  publisher.publish(requestChannelKey(auctionId), JSON.stringify(evt));
}

// forward request to auction runner process
function onUserBidRequest(user, auctionId, bidValue) {
  const evt = {
    name: 'request_bid',
    user_id: user.id,
    bid : bidValue,
  };
  publisher.publish(requestChannelKey(auctionId), JSON.stringify(evt));
}


wss.on('connection', function connection(ws) {

  const newUser = {
    id : shortid.generate(),
  };
  users.set(ws, newUser);

  ws.on('message', function fn(payload) {
    // TODO: validate inputs
    const user = findUser(ws);
    const params = payload.split(' ');
    const action = params[0];
    const param1 = params[1];
    let param2;
    if (params.length > 1) param2 = params[2];
    switch (action) {
      case 'name':
        onUserNameRequest(user, param1); // param1 must be requested name
        break;
      case 'join':
        onUserJoinRequest(user, param1); // param1 must be auction id
        break;
      case 'leave':
        onUserLeaveRequest(user, param1); // param1 must be auction id
        break;
      case 'bid':
        onUserBidRequest(user, param1, param2); // param1: auction id, param2: bid value
        break;
      default:
        logger.error('Unknown user action type:', action);
    }

  });

});

module.exports.startUp = function () {
  // TODO: be able to rejoin an ongoing auction

  // subscribe to global channel
  subscribeToChannel('auction:channels:global');
};
